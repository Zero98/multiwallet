package wallet

type KeyPurpose int

type CoinType uint32

const (
Bitcoin     CoinType = iota
BitcoinCash
Ethereum
)

var DefaultWalletMap = map[CoinType]bool{
Bitcoin :     true,
BitcoinCash : true,
Ethereum :    true,
}

func (c *CoinType) String() string {

switch *c {
case Bitcoin:
return "Bitcoin"
case BitcoinCash:
return "Bitcoin Cash"
case Ethereum:
return  "Ethereum"
default:
return ""
}
}
